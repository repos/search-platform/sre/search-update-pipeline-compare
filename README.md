# search-update-pipeline-compare

A simple script to check consistency of Elasticsearch documents. Based on complare-cluster.py from the [mediawiki-extensions-CirrusSearch repository](https://github.com/wikimedia/mediawiki-extensions-CirrusSearch/blob/master/scripts/compare-clusters.py).
